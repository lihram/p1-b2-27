/*
 * =====================================================================================
 *
 *    Description:  Header file for the genetic algorithm. Here, structs population and individual are defined.
 *
 *        Version:  1.0
 *
 *         Author:  Hilmar Gústafsson, hgusta16@student.aau.dk
 *
 * =====================================================================================
 */

#ifndef GENETIC_H
#define GENETIC_H

/*
 * =======================
 * = .H FILES
 * =======================
 */

 #include "database.h"

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  timeslot
 *
 *       Fields:  id
 *                class     - Pointer to single class
 *                teacher   - Pointer to a single teacher
 *                room      - Pointer to a single room
 *                subjects  - Pointer to a single subject.
 *                startTime - The starting time of the class (American army time format, e.g. 1300 hours.)
 *                duration  - How many minutes the lesson takes.
 *
 *  Description:  The smallest element of an individual, a timeslot is what forms a day.
 *
 * =====================================================================================
 */

typedef struct timeslot
{
    unsigned int id;
    dbClass *klass;
    dbTeacher *teacher;
    dbRoom *room;
    dbSubject *subject;
    unsigned int startTime;
    unsigned int duration;

} timeslot;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  day
 *
 *       Fields:  id            - unique identifier
 *                timeSlots     - an array of timeslots
 *                timeSlotCount - for keeping track of the array
 *                startTime     - Time when the day starts.
 *                duration      - How long the schoolday is.
 *
 *  Description:  One of the five weekdays that form a week.
 *
 * =====================================================================================
 */

typedef struct day
{
    unsigned int id;
    timeslot *timeslots; /* array */
    unsigned int timeslotCount;
    unsigned int startTime;
    unsigned int duration;
    /*
        TODO:
            More attributes for fitness evaluation?
    */
} day;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  week
 *
 *       Fields:  id    - unique identifier
 *                days  - An array of the workdays in a single week.
 *
 *  Description:  One of the weeks that forms an individual.
 *
 * =====================================================================================
 */

typedef struct week
{
    unsigned int id;
    day days[5];
} week;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  individual
 *
 *       Fields:  id    - unique identifier
 *                weeks - The individual should contain a week for each class
 *
 *  Description:  
 *
 * =====================================================================================
 */

typedef struct individual
{
    unsigned int id;
    week *weeks;
    int fitness;
} individual;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  population
 *
 *       Fields:  individuals   - an array of all the individuals of a generation
 *                children      - a location where you can store the next generation
 *                mutationRate  - 1-5, how many days are affected by mutation
 *                crossoverRate - 1-5, how many days are affected by crossover
 *
 *  Description:  The population stores information needed for the algorithm to move 
 *                from one generation to the next.
 *                This includes an array of the current generation, an array where to
 *                store the next generation. It also includes all the necessary numbers
 *                for calculating fitness and reading gene data.
 *
 * =====================================================================================
 */

typedef struct population
{
    individual *individuals;
    individual *children;
    unsigned int mutationRate;
    unsigned int crossoverRate;
    unsigned int size;
} population;

/*
 * ===  SECTION  =======================================================================
 * ============== PROTOTYPES
 * =====================================================================================
 */

population *initializePopulation(unsigned int popSize, unsigned int mutationRate, unsigned int crossoverRate, database *db); /* Check */
int freePopulation(population *pop, database db); /* DONE */

int initializeIndividual(individual *x, database *db); /* DONE */
int freeIndividual(individual *x, database db); /* DONE */
int singleCrossover(individual *x, individual *y, individual *offspring, population *pop, database db);
/**
 * SKIPPED FOR SIMPLICITY
 * int mutate(individual *x,  population *pop); 
 */
int indivCompare(const void *ep1, const void *ep2);

int getFitness(individual *indiv, database db);
int newGeneration(population *pop, database *db);

#endif
