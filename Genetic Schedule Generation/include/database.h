/*
 * =====================================================================================
 *
 *    Description:  Communicate with the basic SQL-themed database located 
 *                  in ../Data/ folder.
 *
 *        Version:  1.0
 *
 *         Author:  Hilmar Gústafsson, hgusta16@student.aau.dk
 *
 * =====================================================================================
 */
#ifndef DATABASE_H
#define DATABASE_H

/*
 * =======================
 * = STANDARD LIBRARIES
 * =======================
 */
#include <stdio.h>
#include <stdlib.h>

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  class
 *
 *       Fields:  ID    - unique identifier for class
 *                grade - between 1-10
 *
 *  Description:  A group of students.
 *
 * =====================================================================================
 */

typedef struct {
    unsigned int id;
    unsigned int grade;
} dbClass;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  roomType
 *
 *       Fields:  id - unique identifier
 *                type - e.g. physics, metalworking, etc.
 *
 *  Description:  The type of room.
 *
 * =====================================================================================
 */

typedef struct dbRoomType {
    unsigned int id;
} dbRoomType;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  room
 *
 *       Fields:  id   - unique identifier
 *               *type - pointer to roomtype
 *
 *  Description:  a room in which a teaching session can take place
 *
 * =====================================================================================
 */

typedef struct dbRoom {
    unsigned int id;
    dbRoomType *type;
} dbRoom;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  subjectHourCount
 *
 *       Fields:  id    - unique identifier
 *                grade - to which grade the hours apply
 *                hours - the amount of hours a specific subject should have
 *
 *  Description:  Documentation of hour requirements for each class
 *
 * =====================================================================================
 */


typedef struct dbSubjectHourCount{
    unsigned int id;
    unsigned int grade;
    unsigned int hours;
} dbSubjectHourCount;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  subject
 *
 *       Fields:  id    - unique identifier
 *               *type  - room needs
 *               *hours - an array of hours required
 *
 *  Description:  The subject taught to classes by teachers
 *
 * =====================================================================================
 */

typedef struct dbSubject {
    unsigned int id;
    dbRoomType *type;
    dbSubjectHourCount *hours;
} dbSubject;

/*
 * ===  STRUCT  ========================================================================
 *
 *         Name:  teacher
 *
 *       Fields:  id        - unique identifier
 *               *primary   - Pointer to primary subject
 *               *secondary - pointer to secondary subject
 *
 *  Description:  A necessary element in any class-teacher lesson
 *
 * =====================================================================================
 */

typedef struct dbTeacher {
    unsigned int id;
    dbSubject *primary;
    dbSubject *secondary;
} dbTeacher;


typedef struct database {
    unsigned int classCount;
    unsigned int roomCount;
    unsigned int roomTypeCount;
    unsigned int subjectCount;
    unsigned int subjectHoursCount;
    unsigned int teacherCount;
    dbClass            *classes;
    dbRoom             *rooms;
    dbRoomType         *roomTypes;
    dbSubject          *subjects;
    dbSubjectHourCount *subjectHours;
    dbTeacher          *teachers;
} database;

/*
 * =======================
 * = ENUMS
 * =======================
 */

 enum dbType {
     classes,
     rooms,
     roomTypes,
     subjects,
     subjectHours,
     teachers
 };


/*
 * ===  SECTION  =======================================================================
 * ============== FUNCTION PROTOTYPES
 * =====================================================================================
 */

database *initializeDatabase();
int freeDB(database *db);

int get(FILE *fp, database *db, const char *filename, enum dbType type);
int readClasses(FILE *fp, database *db);
int readRooms(FILE *fp, database *db);
int readRoomTypes(FILE *fp, database *db);
int readSubjects(FILE *fp, database *db);
int readSubjectHourCounts(FILE *fp, database *db);
int readTeachers(FILE *fp, database *db);
int countItems(FILE *fp);

int printClasses(database *db);
int printRooms(database *db);
int printRoomTypes(database *db);
int printSubject(database *db);
int printSubjectHours(database *db);
int printTeachers(database *db);

#endif
