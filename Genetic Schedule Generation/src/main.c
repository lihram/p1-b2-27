/*
 * =====================================================================================
 *
 *    Description:  
 *
 *        Version:  
 *
 *         Author:  Hilmar Gústafsson, hgusta16@student.aau.dk
 *
 * =====================================================================================
 */

/*
 * =======================
 * = HEADER FILES
 * =======================
 */

#include "genetic-algorithm.h"
#include "database.h"

/*
 * =======================
 * = STANDARD LIBRARIES
 * =======================
 */

#include <stdlib.h>
#include <stdio.h>

/*
 * =======================
 * = MACROS
 * =======================
 */
#define GENERATIONS 400

/*
 * ===  SECTION  =======================================================================
 * ============== FUNCTIONS
 * =====================================================================================
 */

int main(void)
{
  database *db;
  population *pop;
  int i;

  db = initializeDatabase();
  pop = initializePopulation(40, 3, 3, db);
  for (i = 0; i < GENERATIONS; i++)
  {
    newGeneration(pop, db);
  }

  
  freeDB(db);
  freePopulation(pop, *db);

  return EXIT_SUCCESS;
}
