/*
 * =====================================================================================
 *
 *    Description:  
 *
 *        Version:  1.0
 *
 *         Author:  Hilmar Gústafsson, hgusta16@student.aau.dk
 *
 * =====================================================================================
 */

/*
 * =======================
 * = HEADER FILES
 * =======================
 */

#include "../include/database.h"
#include "../include/genetic-algorithm.h"

/*
 * =======================
 * = STANDARD LIBRARIES
 * =======================
 */
 
#include <time.h>

/*
 * ===  SECTION  =======================================================================
 * ============== FUNCTIONS
 * =====================================================================================
 */


/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  initializePopulation
 *
 *    Arguments:  popSize - The size of the population.
 *                mutationRate - How big of a change the mutations will make.
 *                crossoverRate - How big of a change the crossovers will make.
 *                db - The information read from the files about the specific schools.
 *
 *  Description:  Initializes two generations of individuals for testing.
 *
 * =====================================================================================
 */
population *initializePopulation(unsigned int popSize, unsigned int mutationRate, unsigned int crossoverRate, database *db)
{
  population *newPopulation;
  int i;

  newPopulation = malloc(sizeof(population));
  newPopulation->individuals = malloc(sizeof(individual) * popSize);
  newPopulation->children = malloc(sizeof(individual) * popSize);
  newPopulation->mutationRate = mutationRate;
  newPopulation->crossoverRate = crossoverRate;
  newPopulation->size = popSize;

  for (i = 0; i < popSize; i++)
  {
    initializeIndividual(&newPopulation->children[i], db);
  }
  newGeneration(newPopulation, db);

  return newPopulation;
}

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  freePopulation
 *
 *    Arguments:  pop - The population to be freed.
 *                db - The information read from the files.
 *                Needed for underfunctions.
 *
 *  Description:  
 *
 * =====================================================================================
 */
int freePopulation(population *pop, database db)
{
  int i;
  for(i = 0; i < pop->size; i++)
  {
    freeIndividual(&pop->individuals[i], db);
    freeIndividual(&pop->children[i], db);
  }
  free(pop);

  return EXIT_SUCCESS;
}

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  initializeIndividual
 *
 *    Arguments:  x - Individual to initialize
 *                db - Information from file, needed for e.g. classCount, (i.e. how many
 *                different weeks there should be.)
 *
 *  Description:  Creates an individual with 16 schedules, one for each class that exists.
 *
 * =====================================================================================
 */

int initializeIndividual(individual *x, database *db)
{
  int i;
  srand((unsigned) time(NULL));

  x->weeks = malloc(sizeof(week) * db->classCount);

  for(i = 0; i < db->classCount; i++)
  {
    int j;
    week *pweek = &x->weeks[i];
    pweek->id = i;

    for(j = 0; j < 5; j++)
    {
      int k;
      day *pday = &pweek->days[j];
      pday->id = j;
      pday->timeslotCount = rand() % 6 + 3; /* between 3 and 8 */
      pday->timeslots = malloc(sizeof(timeslot) * pday->timeslotCount);
      pday->duration = 0;


      for(k = 0; k < pday->timeslotCount; k++)
      {
        timeslot *ptimeslot = &pday->timeslots[k];
        ptimeslot->id = k;
        ptimeslot->duration = 60; /* TODO: Calculate FROM total hours & random elements */
        pday->duration += ptimeslot->duration;
        ptimeslot->klass = &db->classes[i];
        ptimeslot->teacher = &db->teachers[i % (db->teacherCount + 1)]; /* TODO: Use primary & secondary subject elements, search if space is already occupied for that time in the schedule. */
        ptimeslot->room = &db->rooms[i % (db->roomCount + 1)]; /* Find a room based off of time schedule AND room type necessary. */
        ptimeslot->subject = &db->subjects[i % (db->subjectCount + 1)]; /* Select the one with the highest rating. */
        if(k == 0)
        {
          /* First element */
          ptimeslot->startTime = 800;
        }
        else
        {
          /* Not first. */
          ptimeslot->startTime = pday->timeslots[k-1].startTime + (pday->timeslots[k-1].duration / 0.6);
        }
      }
    }
  }

  getFitness(x, *db);

  return EXIT_SUCCESS;
}


int freeIndividual(individual *x, database db)
{
  int i;
  for (i = 0; i < db.classCount; i++)
  {
    int j;
    for (j = 0; j < 5; j++)
    {
      free(x->weeks[i].days[j].timeslots);
    }
    free(x->weeks);
  }
  free(x);

  return EXIT_SUCCESS;
}

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  singleCrossover
 *
 *    Arguments:  x - Parent 1, crossoverRate determines how many days of each week
 *                are taken from parent x to give to offspring
 *                y - Parent 2, the rest of the days are taken from here.
 *                pop - The population, needed for size information.
 *                db - information read from files.
 *
 *  Description:  Crosses two individual schemas together into one
 *
 * =====================================================================================
 */

int singleCrossover(individual *x, individual *y, individual *offspring, population *pop, database db)
{
  int i;
  for (i = 0; i < 5; i++)
  {
    int j;
    for (j = 0; j < db.classCount; j++)
    {
      day temp;

      if(i < pop->size)
      {
        temp = x->weeks[j].days[i];
      }
      else
      {
        temp = y->weeks[j].days[i];
      }
      
      offspring->weeks[j].days[i] = temp;
    }
  }
  return EXIT_SUCCESS;
}
/* SKIPPED FOR SIMPLICITY
int mutate(individual *x, population *pop)
{

}
*/

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  indivCompare
 *
 *    Arguments:  ep1 - Element pointer 1, for comparison of individuals
 *                ep1 - Element pointer 2, same purpose.
 *
 *  Description:  Orders an array of inviduals based on fitness.
 *
 * =====================================================================================
 */

int indivCompare(const void *ep1, const void *ep2)
{
  individual  *pi1 = (individual *)ep1, 
              *pi2 = (individual *)ep2;

  if(pi2->fitness > pi1->fitness)
  {
    return 1;
  }
  else if (pi2->fitness == pi1->fitness)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  getFitness
 *
 *    Arguments:  indiv - The individual the fitness value will be given to.
 *                db - Information needed from the files to analyse data.
 *
 *  Description:  Changes the fitness value of a given individual.
 *
 * =====================================================================================
 */

int getFitness(individual *indiv, database db)
{
  int i;
  indiv->fitness = 0;
  for(i = 0; i < db.classCount; i++)
  {
    /* 
        Basic example of functionality, not the complete edition. 
        Days are rated by duration, whereas ideally they'd be rated 
        by hard and soft conditions,
        for example whether or not something is double booked, (hard condition),
        or whether or not there is variety in a day. (Soft condition).
        Hard conditions ALWAYS lead to a bad fitness rating, whereas 
        soft conditions may just decrease it a little.

     */
    int j;
    week temp = indiv->weeks[i];
    for(j = 0; j < 5; j++)
    {
      indiv->fitness -= temp.days[i].duration;
    }
  }
  return EXIT_SUCCESS;
}

/*
 * ===  FUNCTION  ======================================================================
 *
 *         Name:  newGeneration
 *
 *    Arguments:  pop - The population which will be given a new generation.
 *                db - The database that contains the necessary information
 *
 *  Description:  Makes the current children the parents, 
 *                and generates more children from them.
 *
 * =====================================================================================
 */

int newGeneration(population *pop, database *db)
{
  int i;
  srand((unsigned) time(NULL));
  pop->individuals = pop->children;
  qsort(pop->individuals, pop->size, sizeof(individual), indivCompare); /* Sort parents by fitness. */

  for(i = 0; i < pop->size; i++) /* Make a new generation from random parents of the better slice. */
  {
    singleCrossover(&pop->individuals[rand() % (pop->size / 2)], &pop->individuals[rand() % (pop->size / 2)], &pop->children[i], pop, *db);
  }
  return EXIT_SUCCESS;
}


