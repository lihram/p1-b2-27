/*
 * =====================================================================================
 *
 *    Description:  ----
 *
 *        Version:  1.0
 *
 *         Author:  Hilmar Gústafsson, hgusta16@student.aau.dk
 *
 * =====================================================================================
 */

/*
 * =======================
 * = HEADER FILES
 * =======================
 */

#include "../include/database.h"

/*
 * =======================
 * = MACRO FILEPATHS
 * =======================
 */
#define TABLE_CLASS         "../../Data/TABLE_CLASS"
#define TABLE_ROOM          "../../Data/TABLE_ROOM"
#define TABLE_ROOM_TYPE     "../../Data/TABLE_ROOM_TYPE"
#define TABLE_SUBJECT       "../../Data/TABLE_SUBJECT"
#define TABLE_SUBJECT_HOURS "../../Data/TABLE_SUBJECT_HOURS"
#define TABLE_TEACHER       "../../Data/TABLE_TEACHER"

/*
 * ===  SECTION  =======================================================================
 * ============== FUNCTIONS
 * =====================================================================================
 */

database *initializeDatabase()
{
  database *newDatabase = malloc(sizeof(database));
  FILE *fp = NULL;
  enum dbType currentTable;
  
  get(fp, newDatabase, TABLE_CLASS, currentTable = classes);
  get(fp, newDatabase, TABLE_ROOM_TYPE, currentTable = roomTypes);
  get(fp, newDatabase, TABLE_ROOM, currentTable = rooms);
  get(fp, newDatabase, TABLE_SUBJECT_HOURS, currentTable = subjectHours);
  get(fp, newDatabase, TABLE_SUBJECT, currentTable = subjects);
  get(fp, newDatabase, TABLE_TEACHER, currentTable = teachers);

  return newDatabase;
}

int freeDB(database *db)
{
  int i;

  free(db->classes);
  free(db->roomTypes);
  free(db->subjectHours);
  
  for(i = 0; i < db->roomCount; i++)
  {
    free(db->rooms[i].type);
  }
  free(db->rooms);
  
  for(i = 0; i < db->subjectCount; i++)
  {
    free(db->subjects[i].type);
    free(db->subjects[i].hours);
  }
  free(db->subjects);

  for(i = 0; i < db->teacherCount; i++)
  {
    free(db->teachers[i].primary);
    free(db->teachers[i].secondary);
  }
  free(db->teachers);

  free(db);

  return EXIT_SUCCESS;
}

int get(FILE *fp, database *db, const char *filename, enum dbType type)
{
  fp = fopen(filename, "r");
  if (fp != NULL)
  {
    switch (type)
    {
    case classes:
      return readClasses(fp, db);
    case rooms:
      return readRooms(fp, db);
    case roomTypes:
      return readRoomTypes(fp, db);
    case subjects:
      return readSubjects(fp, db);
    case subjectHours:
      return readSubjectHourCounts(fp, db);
    case teachers:
      return readTeachers(fp, db);
    default:
      break;
    }
  }
  /* else.. */
  return EXIT_FAILURE;
}

int readClasses(FILE *fp, database *db)
{
  dbClass temp;
  char line[100];

  db->classCount = countItems(fp);
  if (db->classCount > 0)
  {
    int counter = 0;
    db->classes = malloc(sizeof(dbClass) * db->classCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u %3u", &temp.id, &temp.grade) == 2)
      {
        db->classes[counter++] = temp;
      }
    }
    return counter;
  }
  /* else... */
  return 0;
}

int readRoomTypes(FILE *fp, database *db)
{
  dbRoomType temp;
  char line[100];

  db->roomTypeCount = countItems(fp);
  if (db->roomTypeCount > 0)
  {
    int counter = 0;
    db->roomTypes = malloc(sizeof(dbRoomType) * db->roomTypeCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u", &temp.id) == 1)
      {
        db->roomTypes[counter++] = temp;
      }
    }
    return counter;
  }

  /* else... */
  return 0;
}

int readRooms(FILE *fp, database *db)
{
  dbRoom temp;
  char line[100];

  db->roomCount = countItems(fp);
  if (db->roomCount > 0)
  {
    int counter = 0;
    db->rooms = malloc(sizeof(dbRoom) * db->roomCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      unsigned int roomType_fk = 0;
      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u %3u", &temp.id, &roomType_fk) == 2)
      {
        int i;
        db->rooms[counter] = temp;
        for (i = 0; i < db->roomTypeCount; i++)
        {
          if (db->roomTypes[i].id == roomType_fk)
          {
            db->rooms[counter].type = &db->roomTypes[i];
            break;
          }
        }
        counter++;
      }
    }
    return counter;
  }

  /* else... */
  return 0;
}

int readSubjects(FILE *fp, database *db)
{
  dbSubject temp;
  char line[100];

  db->subjectCount = countItems(fp);
  if (db->subjectCount > 0)
  {
    int counter = 0;
    db->subjects = malloc(sizeof(dbSubject) * db->subjectCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      unsigned int  roomType_fk = 0,
                    subjectHours_fk = 0;

      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u %3u %3u", &temp.id, &roomType_fk, &subjectHours_fk) == 3)
      {
        int i;
        db->subjects[counter] = temp;
        for (i = 0; i < db->roomTypeCount; i++)
        {
          if (db->roomTypes[i].id == roomType_fk)
          {
            db->subjects[counter].type = &db->roomTypes[i];
            break;
          }
        }
        for (i = 0; i < db->subjectHoursCount; i++)
        {
          if (db->subjectHours[i].id == subjectHours_fk)
          {
            db->subjects[counter].hours = &db->subjectHours[i];
            break;
          }
        }
        counter++;
      }
    }
    return counter;
  }

  /* else... */
  return 0;
}

int readSubjectHourCounts(FILE *fp, database *db)
{
  dbSubjectHourCount temp;
  char line[100];

  db->subjectHoursCount = countItems(fp);
  if (db->subjectHoursCount > 0)
  {
    int counter = 0;
    db->subjectHours = malloc(sizeof(dbSubjectHourCount) * db->subjectHoursCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u %3u %3u", &temp.id, &temp.grade, &temp.hours) == 3)
      {
        db->subjectHours[counter++] = temp;
      }
    }
    return counter;
  }
  /* else... */
  return 0;
}

int readTeachers(FILE *fp, database *db)
{
  dbTeacher temp;
  char line[100];

  db->teacherCount = countItems(fp);
  if (db->teacherCount > 0)
  {
    int counter = 0;
    db->teachers = malloc(sizeof(dbTeacher) * db->teacherCount);
    rewind(fp);

    while (fgets(line, sizeof(line), fp) != NULL)
    {
      unsigned int  subject_1_fk = 0,
                    subject_2_fk = 0;

      if (*line == '#')
      {
        continue;
      }
      else if (sscanf(line, "%3u %3u %3u", &temp.id, &subject_1_fk, &subject_2_fk) == 3)
      {
        int i, j = 0;
        db->teachers[counter] = temp;
        for (i = 0; i < db->subjectCount; i++)
        {
          if(j >= 2)
          {
            break;
          }
          else if(db->subjects[i].id == subject_1_fk)
          {
            db->teachers[counter].primary = &db->subjects[i];
            j++;
          }
          else if(db->subjects[i].id == subject_2_fk)
          {
            db->teachers[counter].secondary = &db->subjects[i];
            j++;
          }
        }
        counter++;
      }
    }
    return counter;
  }

  /* else... */
  return 0;
}

int countItems(FILE *fp)
{
  int counter = 0;
  char buffer[100];
  while (fgets(buffer, sizeof(buffer), fp) != NULL)
  {
    if (*buffer == '#')
    {
      continue;
    }
    else
    {
      counter++;
    }
  }
  return counter;
}

int printClasses(database *db)
{
  int i;
  printf("Printing classes: \n");
  for(i = 0; i < db->classCount; i++)
  {
    printf("%03u %03u\n", db->classes[i].id, db->classes[i].grade);
  }
  return EXIT_SUCCESS;
}

int printRooms(database *db)
{
  int i;
  printf("Printing rooms: \n");
  for(i = 0; i < db->roomCount; i++)
  {
    printf("%03u %03u\n", db->rooms[i].id, db->rooms[i].type->id);
  }
  return EXIT_SUCCESS;
}

int printRoomTypes(database *db)
{
  int i;
  printf("Printing room types: \n");
  for(i = 0; i < db->roomTypeCount; i++)
  {
    printf("%03u\n", db->roomTypes[i].id);
  }
  return EXIT_SUCCESS;
}

int printSubject(database *db)
{
  int i;
  printf("Printing subjects: \n");
  for(i = 0; i < db->subjectCount; i++)
  {
    printf("%03u %03u %03u\n", db->subjects[i].id, db->subjects[i].type->id, db->subjects[i].hours->hours);
  }
  return EXIT_SUCCESS;
}

int printSubjectHours(database *db)
{
  int i;
  printf("Printing subject hours: \n");
  for(i = 0; i < db->subjectHoursCount; i++)
  {
    printf("%03u %03u %03u\n", db->subjectHours[i].id, db->subjectHours[i].hours, db->subjectHours[i].grade);
  }
  return EXIT_SUCCESS;
}

int printTeachers(database *db)
{
  int i;
  printf("Printing teachers: \n");
  for(i = 0; i < db->teacherCount; i++)
  {
    printf("%03u %03u %03u\n", db->teachers[i].id, db->teachers[i].primary->id, db->teachers[i].secondary->id);
  }
  return EXIT_SUCCESS;
}
